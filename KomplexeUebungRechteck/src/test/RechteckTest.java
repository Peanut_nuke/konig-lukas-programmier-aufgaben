package test;

import model.Rechteck;
import controller.BunteReckteckeController;

public class RechteckTest {

	public static void main(String[] args) {
		Rechteck rechteck0 = new Rechteck();
		rechteck0.setX(10);
		rechteck0.setY(10);
		rechteck0.setBreit(30);
		rechteck0.setHoehe(40);

		Rechteck rechteck1 = new Rechteck();
		rechteck1.setX(25);
		rechteck1.setY(25);
		rechteck1.setBreit(100);
		rechteck1.setHoehe(20);

		Rechteck rechteck2 = new Rechteck();
		rechteck2.setX(260);
		rechteck2.setY(10);
		rechteck2.setBreit(200);
		rechteck2.setHoehe(100);

		Rechteck rechteck3 = new Rechteck();
		rechteck3.setX(5);
		rechteck3.setY(500);
		rechteck3.setBreit(300);
		rechteck3.setHoehe(25);

		Rechteck rechteck4 = new Rechteck();
		rechteck4.setX(100);
		rechteck4.setY(100);
		rechteck4.setBreit(100);
		rechteck4.setHoehe(100);

		Rechteck rechteck5 = new Rechteck(200, 200, 200, 200);
		Rechteck rechteck6 = new Rechteck(800, 400, 20, 20);
		Rechteck rechteck7 = new Rechteck(800, 450, 20, 20);
		Rechteck rechteck8 = new Rechteck(850, 400, 20, 20);
		Rechteck rechteck9 = new Rechteck(855, 455, 25, 25);
		Rechteck rechteck10 = new Rechteck(-4, -5, -50, -200);

		System.out.println(rechteck10);

		Rechteck rechteck11 = new Rechteck();
		rechteck11.setX(-10);
		rechteck11.setY(-10);
		rechteck11.setBreit(-200);
		rechteck11.setHoehe(-100);

		System.out.println(rechteck11);

		BunteReckteckeController brc = new BunteReckteckeController();
		brc.add(rechteck0);
		brc.add(rechteck1);
		brc.add(rechteck2);
		brc.add(rechteck3);
		brc.add(rechteck4);
		brc.add(rechteck5);
		brc.add(rechteck6);
		brc.add(rechteck7);
		brc.add(rechteck8);
		brc.add(rechteck9);
		brc.add(rechteck10);

	}

	public static void rechteckeTesten() {

		Rechteck[] rechteck = new Rechteck[50000];
		boolean ueberpruefe = true;
		Rechteck rechteckBig = new Rechteck(0, 0, 1200, 1000);
		for (int i = 0; i < rechteck.length; i++) {
			rechteck[i] = Rechteck.generiereZufallsRechteck();
			if (rechteckBig(rechteck[i])) {
				System.out.println(rechteck[i]);
				ueberpruefe = false;
			}
		}
	}

	private static boolean rechteckBig(Rechteck rechteck) {
		// TODO Auto-generated method stub
		return false;
	}

}
	