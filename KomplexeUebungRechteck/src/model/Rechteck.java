package model;

public class Rechteck {
	// Attribute
	private Punkt p;
	private int Breit;
	private int hoehe;

	public Rechteck() {
		this.p = new Punkt();
		this.Breit = 0;
		this.hoehe = 0;
		// TODO Auto-generated constructor stub
	}

	public Rechteck(int x, int y, int Breit, int hoehe) {
		super();
		this.p = new Punkt(x,y);
		this.Breit = Breit;
		this.hoehe = hoehe;
	}

	public int getX() {
		return this.p.getX(); 
	}

	public void setX(int x) {
		this.p.setX(x);
	}

	public int getY() {
		return this.p.getY();
	}

	public void setY(int y) {
		this.p.setY(y);
	}

	public int getBreit() {
		return Breit;
	}

	public void setBreit(int Breit) {
		this.Breit = Math.abs(Breit);
	}

	public int getHoehe() {
		return hoehe;
	}

	public void setHoehe(int hoehe) {
		this.hoehe = Math.abs(hoehe);
	}

	@Override
	public String toString() {
		return "Rechteck [x=" + x + ", y=" + y + ", Breit=" + Breit + ", hoehe=" + hoehe + "]";
	}

	
	public boolean enthaelt(int x, int y) {
		return this.p.getX() <= x && x <= this.p.getX() + this.Breit && this.p.getY() <= y && y <= this.p.getY() + this.hoehe; 
	}
	
	public boolean enthaelt(Punkt p) {
	  return enthaelt (p.getX(),p.getY());
	}
	public boolean enthaelt(Rechteck rechteck) {
		//keine Ahnung
	}
	public static Rechteck generiereZufallsRechteck() {
		//keine Ahnung
	}
}
