package model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

public class MySQLDatabase {

	private final String driver = "com.mysql.jbc.Driver";
	private final String url = "jdbc:mysql://localhost/rechtecke?";
	private final String user = "root";
	private final String passwort ="";
	
	public void rechteckeEintragen(Rechteck r) {
		try {
			Class.forName(driver);
			Connection con = DriverManager.getConnection(url, user, passwort);
			String sql ="INSERT INTO T_Rechtecke(x,y,breit,hoehe) VALUES\r\n" + 
					"(?,?,?,?);";	
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, r.getX());
			ps.setInt(2, r.getY());
			ps.setInt(3, r.getBreit());
			ps.setInt(4, r.getHoehe());
			ps.executeUpdate();
			
			
				con.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
			
		
	}
	public List<Rechteck> getAlleRechtecke(){
		List <Rechteck> rechtecke = new LinkedList<Rechteck>();
		try {
			Class.forName(driver);
			Connection con = DriverManager.getConnection(url, user, passwort);
			String sql = "SELECT * FROM T_Rechtecke;";
			Statement s = con.createStatement();
			ResultSet rs = s.executeQuery(sql);
			while(rs.next()) {
				int x = rs.getInt("x");
				int y = rs.getInt("y");
				int breit = rs.getInt("breit");
				int hoehe = rs.getInt("hoehe");
				rechtecke.add(new Rechteck(x,y,breit,hoehe));
			}
			
			con.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return rechtecke;
	}
}
