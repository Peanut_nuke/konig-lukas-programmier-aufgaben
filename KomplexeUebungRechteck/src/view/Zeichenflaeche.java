package view;

import view.JPanel;

import java.awt.Color;
import java.awt.Graphics;
import model.Rechteck;
import controller.BunteReckteckeController;

public class Zeichenflaeche extends JPanel {
 private BunteReckteckeController brc;
 public Zeichenflaeche(BunteReckteckeController brc) {
	 this.brc = brc;
 }
 public void paintComponent(Graphics g) {
	 g.setColor(Color.BLACK);
	 for (Rechteck eck : brc.getRechtecke()) {
			g.fillRect(eck.getX(), eck.getY(), eck.getBreit(), eck.getHoehe());
		}
	 
 }

	
	
	
}
